<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class jdUraian extends Model
{
   	public $timestamps = true;
    protected $table = 'tbl_jduraian';

    protected $fillable = ['id_jduraian','nm_jd'];
}
