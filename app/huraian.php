<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class huraian extends Model
{
	public $timestamps = true;
    protected $table = 'tbl_huraian';

    protected $fillable = ['id_huraian','id_hrab','id_jduraian','nama_uraian','sub_total'];
}
