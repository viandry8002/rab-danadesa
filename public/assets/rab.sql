-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 03 Mar 2020 pada 09.10
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.3.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rab`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2020_03_03_025712_create_hrab_table', 2),
(4, '2020_03_03_030245_create_huraian_table', 3),
(5, '2020_03_03_030306_create_jduraian_table', 3),
(6, '2020_03_03_030322_create_uraian_table', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_hrab`
--

CREATE TABLE `tbl_hrab` (
  `id_hrab` int(20) NOT NULL,
  `desa` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kecamatan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kabupaten` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provinsi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bidang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kegiatan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `panjang` int(11) NOT NULL,
  `lebar` int(11) NOT NULL,
  `tebal` int(11) NOT NULL,
  `volume` int(11) NOT NULL,
  `total_biaya` int(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Trigger `tbl_hrab`
--
DELIMITER $$
CREATE TRIGGER `before_delete_rab` BEFORE DELETE ON `tbl_hrab` FOR EACH ROW BEGIN
DELETE FROM tbl_uraian WHERE id_huraian IN(
    SELECT id_huraian
    FROM tbl_huraian where 
	id_hrab = old.id_hrab);

DELETE FROM tbl_huraian WHERE id_hrab = old.id_hrab;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_huraian`
--

CREATE TABLE `tbl_huraian` (
  `id_huraian` int(20) NOT NULL,
  `id_hrab` int(11) NOT NULL,
  `id_jduraian` int(11) NOT NULL,
  `sub_total` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_jduraian`
--

CREATE TABLE `tbl_jduraian` (
  `id_jduraian` int(20) NOT NULL,
  `nm_jd` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tbl_jduraian`
--

INSERT INTO `tbl_jduraian` (`id_jduraian`, `nm_jd`, `created_at`, `updated_at`) VALUES
(1, 'Bahan', '2020-03-03 03:49:06', '2020-03-03 03:49:06'),
(2, 'Alat', '2020-03-03 03:49:19', '2020-03-03 03:49:19'),
(3, 'Upah', '2020-03-03 03:49:43', '2020-03-03 03:49:43'),
(4, 'ATK', '2020-03-03 03:49:43', '2020-03-03 03:49:43'),
(5, 'TPK', '2020-03-03 03:49:43', '2020-03-03 03:49:43');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_uraian`
--

CREATE TABLE `tbl_uraian` (
  `id_uraian` int(20) NOT NULL,
  `id_huraian` int(11) NOT NULL,
  `uraian` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `volume` int(11) NOT NULL,
  `satuan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hsatuan` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'dafa', 'dafa@gmail', NULL, '$2y$10$EE0p2KmgPu/mTuMOLXhUQ.h7m3CZ4Re6YEifs/O7DKyl1BWaFwIda', NULL, '2020-03-02 19:54:02', '2020-03-02 19:54:02');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `tbl_hrab`
--
ALTER TABLE `tbl_hrab`
  ADD PRIMARY KEY (`id_hrab`);

--
-- Indeks untuk tabel `tbl_huraian`
--
ALTER TABLE `tbl_huraian`
  ADD PRIMARY KEY (`id_huraian`),
  ADD KEY `id_hrab` (`id_hrab`),
  ADD KEY `id_jduraian` (`id_jduraian`);

--
-- Indeks untuk tabel `tbl_jduraian`
--
ALTER TABLE `tbl_jduraian`
  ADD PRIMARY KEY (`id_jduraian`);

--
-- Indeks untuk tabel `tbl_uraian`
--
ALTER TABLE `tbl_uraian`
  ADD PRIMARY KEY (`id_uraian`),
  ADD KEY `id_huraian` (`id_huraian`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tbl_hrab`
--
ALTER TABLE `tbl_hrab`
  MODIFY `id_hrab` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tbl_huraian`
--
ALTER TABLE `tbl_huraian`
  MODIFY `id_huraian` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tbl_jduraian`
--
ALTER TABLE `tbl_jduraian`
  MODIFY `id_jduraian` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tbl_uraian`
--
ALTER TABLE `tbl_uraian`
  MODIFY `id_uraian` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tbl_huraian`
--
ALTER TABLE `tbl_huraian`
  ADD CONSTRAINT `tbl_huraian_ibfk_1` FOREIGN KEY (`id_hrab`) REFERENCES `tbl_hrab` (`id_hrab`),
  ADD CONSTRAINT `tbl_huraian_ibfk_2` FOREIGN KEY (`id_jduraian`) REFERENCES `tbl_jduraian` (`id_jduraian`);

--
-- Ketidakleluasaan untuk tabel `tbl_uraian`
--
ALTER TABLE `tbl_uraian`
  ADD CONSTRAINT `tbl_uraian_ibfk_1` FOREIGN KEY (`id_huraian`) REFERENCES `tbl_huraian` (`id_huraian`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
