@extends('layouts.app')

@section('content')
  <section class="content">

      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- Default box -->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">RAB</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              
              <div class="card-body">
                <form action="/JudulUraian/Insert" method="POST">
                    {{ csrf_field() }}
                    <div class="row">
                      <div class="col-md-12">
                      <!-- /.form-group -->
                      <div class="form-group">
                        <div class="form-group row">
                        <label  class="col-sm-2 col-form-label">Judul Uraian</label>
                        <div class="col-sm-4">
                          <input type="text" name="nm_jd" class="form-control"  placeholder="Nama Judul Uraian" >
                        </div>
                        </div>
                        <div class="form-group row">
                        <div class="col-sm-10">
                          <button class="btn btn-primary" type="submit" name="submit">Simpan</button>
                        </div>
                        </div>
                      </div>
                      <!-- /.form-group -->
                    </div>
                    </div>
                    
                </form>
              </div>
            
              <!-- /.card-body -->
              <div class="card-footer">
                <!-- Footer -->
              </div>
              <!-- /.card-footer-->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </section>
@endsection